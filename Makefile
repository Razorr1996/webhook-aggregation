up:
	docker-compose up

add_redis_config:
	docker-compose exec redis bash -c "cat /add-config.redis | redis-cli"

send_stripe_hook:
	curl -v -X POST http://localhost:3000/stripe/webhook
