#!/usr/bin/env python3

import argparse
import logging
from pprint import pformat
from typing import Optional, Dict, Any, List
from urllib.parse import urlparse

from redis import Redis

CMD_UPDATE = 'update'
CMD_DELETE = 'delete'
CMD_SET_DEFAULT = 'set_default'

TRAEFIK_PREFIX = 'traefik'

ENTRYPOINT_WEB3000 = 'e-web3000'
ENTRYPOINT_WEB3001 = 'e-web3001'

ROUTE_200OK = 'r-200ok'
ROUTE_200OK_RULE = 'PathPrefix(`/`)'
ROUTE_STRIPE = 'r-stripe'
ROUTE_STRIPE_RULE = 'Path(`/stripe/webhook`)'

MIDDLEWARE_200OK = 'm-always-200ok'

SERVICE_WEB3001 = 's-web3001'
SERVICE_NOOP = 'noop@internal'
SERVICE_AGGREGATION = 's-aggregation'

HEALTHCHECK_INTERVAL = '10m'
HEALTHCHECK_TIMEOUT = '30s'

MIRROR_PERCENT = '100'

HTTP_CODE_200 = '200'

REDIS_DEFAULT = 'redis://localhost:6379'

_ENTRYPOINTS = 'entrypoints'
_HEALTHCHECK = 'healthcheck'
_HTTP = 'http'
_LOADBALANCER = 'loadbalancer'
_MIDDLEWARES = 'middlewares'
_MIRRORING = 'mirroring'
_ROUTERS = 'routers'
_RULE = 'rule'
_SERVERS = 'servers'
_SERVICE = 'service'
_SERVICES = 'services'
_URL = 'url'

DEFAULT_CONFIG = {
    f'{TRAEFIK_PREFIX}/{_HTTP}': {
        _SERVICES: {
            f'{SERVICE_WEB3001}/{_LOADBALANCER}/{_SERVERS}/0/{_URL}': 'http://localhost:3001',
            f'{SERVICE_AGGREGATION}/{_MIRRORING}/{_SERVICE}': SERVICE_WEB3001,
        },
        _MIDDLEWARES: {
            f'{MIDDLEWARE_200OK}/plugin/staticresponse/StatusCode': HTTP_CODE_200,
        },
        _ROUTERS: {
            ROUTE_200OK: {
                _SERVICE: SERVICE_NOOP,
                _RULE: ROUTE_200OK_RULE,
                _ENTRYPOINTS: [ENTRYPOINT_WEB3001],
                _MIDDLEWARES: MIDDLEWARE_200OK,
            },
            ROUTE_STRIPE: {
                _SERVICE: SERVICE_AGGREGATION,
                _RULE: ROUTE_STRIPE_RULE,
                _ENTRYPOINTS: [ENTRYPOINT_WEB3000],
            },
            # # For adding new route in the future:
            # ROUTE_EXAMPLE2: {
            #     _SERVICE: SERVICE_AGGREGATION,
            #     _RULE: ROUTE_EXAMPLE2_RULE,
            #     _ENTRYPOINTS: [ENTRYPOINT_WEB3000],
            # },
        },
    },
}


def add_default_config(redis: Redis):
    logging.info('Set default config')
    set_redis_config(redis, DEFAULT_CONFIG)


def set_redis_config(redis: Redis, config: Dict[str, Any]):
    logging.info(f'Set Redis values in {redis}')
    logging.debug(pformat(config))
    values = flatten_dict(config)
    for k, v in values.items():
        logging.info(f"set '{k}' '{v}'")
        redis.set(k, v)


def del_redis_config(redis: Redis, keys: List[str]):
    logging.info(f'Del Redis values in {redis}')
    logging.info(pformat(keys))
    redis.delete(*keys)


def flatten_dict(value: Any, prefix: str = '', sep: str = '/') -> Dict[str, str]:
    additional_prefix = '' if prefix == '' else f'{prefix}{sep}'
    res = {}
    if isinstance(value, dict):
        for k, v in value.items():
            k_new = f'{additional_prefix}{k}'
            res |= flatten_dict(v, k_new, sep)
    elif isinstance(value, list):
        for e, i in enumerate(value):
            res |= flatten_dict(i, f'{additional_prefix}{e}', sep)
    else:
        res[prefix] = str(value)
    return res


def construct_url(base_url: str, subdomain: Optional[str] = None, path: Optional[str] = None) -> str:
    logging.info(f'{base_url=} {subdomain=} {path=}')
    parse_url = urlparse(base_url, scheme='https')
    logging.info(f'{parse_url=}')
    if subdomain is not None:
        parse_url = parse_url._replace(netloc=f'{subdomain}.{parse_url.netloc}')
    if path is not None:
        old_path = parse_url.path.rstrip('/')
        parse_url = parse_url._replace(path=f'{old_path}{path}')
    logging.info(f'{parse_url=}')
    return parse_url.geturl()


def get_service_name_by_id(service_id: int) -> str:
    return f's-preview-{service_id}'


def construct_service_config(service_id: int, url: str, healthcheck_path: str) -> Dict[str, Any]:
    service_name = get_service_name_by_id(service_id)
    res = {
        f'{TRAEFIK_PREFIX}/{_HTTP}': {
            _SERVICES: {
                f'{service_name}/{_LOADBALANCER}': {
                    f'{_SERVERS}/0/{_URL}': url,
                    _HEALTHCHECK: {
                        'path': healthcheck_path,
                        'interval': HEALTHCHECK_INTERVAL,
                        'timeout': HEALTHCHECK_TIMEOUT,
                    },
                    'passHostHeader': 'false',
                },
                f'{SERVICE_AGGREGATION}/{_MIRRORING}/mirrors/{service_id}': {
                    'name': service_name,
                    'percent': MIRROR_PERCENT,
                },
            },
        },
    }
    return res


def update_service_by_id(redis: Redis, service_id: int, url: str, healthcheck_path: str):
    logging.info(f'Update Traefik service {service_id=} {url=}, {healthcheck_path=}')
    config = construct_service_config(service_id, url, healthcheck_path)
    set_redis_config(redis, config)


def delete_service_by_id(redis: Redis, service_id: int):
    logging.info(f'Delete Traefik service {service_id=}')
    dummy_config = construct_service_config(service_id, 'http://localhost', healthcheck_path='/')
    keys_to_delete = list(flatten_dict(dummy_config).keys())
    del_redis_config(redis, keys_to_delete)


def main():
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description='Manipulate Traefik mirroring for Webhook aggregation')

    parser.add_argument('-r', '--redis', help=f'Redis URL, default value: "{REDIS_DEFAULT}"', default=REDIS_DEFAULT)

    subparsers = parser.add_subparsers(dest='command', required=True)

    sub_update = subparsers.add_parser(CMD_UPDATE, help='Add or update Traefik mirroring service by ID')
    sub_update.add_argument('-i', '--id', required=True, help='Uniq Traefik service ID for project, e.g. Gitlab MR IID')
    sub_update.add_argument('-b', '--base-url', required=True, help='Base URL')
    sub_update.add_argument('-s', '--subdomain', help='Additional subdomain')
    sub_update.add_argument('-d', '--set-default', action='store_true',
                            help='Set default Traefik dynamic config in Redis')
    sub_update.add_argument('--health', required=True, help='Healthcheck path')

    sub_delete = subparsers.add_parser(CMD_DELETE, help='Delete Traefik mirroring service by ID')
    sub_delete.add_argument('-i', '--id', required=True, help='Uniq Traefik service ID for project, e.g. Gitlab MR IID')

    _sub_set_default = subparsers.add_parser(CMD_SET_DEFAULT, help='Set default Traefik dynamic config')

    args = parser.parse_args()
    logging.debug(pformat(args))

    redis_client = Redis.from_url(args.redis)

    if args.command == CMD_UPDATE:
        if args.set_default:
            add_default_config(redis_client)

        url = construct_url(base_url=args.base_url, subdomain=args.subdomain)

        update_service_by_id(redis_client, args.id, url, args.health)
    elif args.command == CMD_DELETE:
        delete_service_by_id(redis_client, args.id)
    elif args.command == CMD_SET_DEFAULT:
        add_default_config(redis_client)


if __name__ == '__main__':
    main()
    pass
